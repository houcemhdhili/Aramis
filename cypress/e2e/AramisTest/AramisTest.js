import {Given, When, Then } from "cypress-cucumber-preprocessor/steps"
import { HomePage } from "../../support/pageObject/homePage"
import { SearchPage } from "../../support/pageObject/searchPage"
import { FinalisationPage } from "../../support/pageObject/FinalisationPage"


const homepage = new HomePage()
const searchpage = new SearchPage()
const finalisationpage = new FinalisationPage()


Given ("User open autohero webSite", ()=>{
    homepage.navigate()

})

When ("User click on search car button",()=>{
    homepage.goToSearchScreen()
})

Then ("the title contains {string}", (title)=>{
    cy.title().should('contain',title)
})

Then ('User search for {string} {string} car',(mark,model)=>{
    searchpage.chooseAudiMark(mark)
    searchpage.chooseA3Model(model)

})

When('choose the third car in the list',()=>{
    searchpage.select3rdCar()
    searchpage.buyNowButton()
})

Then ('User sees the finalisation of the commande screen',()=>{
    finalisationpage.checkGoAheadbuuton()
    finalisationpage.checkTitle()
    finalisationpage.checkscreenText()
})