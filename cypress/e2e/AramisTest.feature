Feature: buy Audi A3 from autohero.com

    Scenario: search for Audi A3 in autohero.com
        Given User open autohero webSite
        When User click on search car button
        Then the title contains "Voitures d'occasion"
        When User search for "Audi" "A3" car
        And choose the third car in the list
        Then User sees the finalisation of the commande screen