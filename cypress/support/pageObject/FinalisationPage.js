export class FinalisationPage{

    checkTitle(){
        cy.title().should('contain','Finalisation de votre commande')
    }

    
    checkGoAheadbuuton(){
        cy.get('#guide-order-next-button').should('be.visible')
    }

    checkscreenText(){
        cy.get('h1').should('be.visible')
    }

}