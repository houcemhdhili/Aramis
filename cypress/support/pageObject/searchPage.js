export class SearchPage {

    chooseAudiMark(mark){
        cy.get('#carMakeFilter').click()
        cy.get('div').contains(mark).click()
    }


    chooseA3Model(model){
        cy.get('#carModelsFilter').click()
        cy.get('[data-qa-selector-value="A3"]').click()
    }

    select3rdCar(){
        cy.get('img[alt="Audi A3"]').eq(3).click()
    }

    buyNowButton(){
        cy.get('[data-qa-selector="buy-now"]').click()
    }

    
}