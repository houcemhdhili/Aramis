export class HomePage{

    navigate(){
        cy.visit('https://www.autohero.com/fr/')
        cy.viewport(1300, 750)
        this.closeCookies()
        cy.get('.logo___p24L5').should('be.visible')
    }

    closeCookies(){
        cy.get('.actions___elpML > .default___1FRAY').click()
    }

    goToSearchScreen(){

        cy.get('a[href="/fr/search/"]').contains('Trouvez votre voiture').click()
    }



}